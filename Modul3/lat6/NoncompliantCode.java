/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1_6;

/**
 *
 * @author dika
 */
public class NoncompliantCode {
    public NoncompliantCode(){
        doLogic();
    }
    
    public void doLogic(){
        System.out.println("This is superclass!");
    }
    
    class SubClass extends NoncompliantCode {
        private String color = null;
        public SubClass(){
            super();
            color = "Red";
        }
        
        public void doLogic(){
            System.out.println("This is subclass! The color is :" + color);
        }
        
        public class Overridable{
            public static void main(String[] args) {
                NoncompliantCode bc = new NoncompliantCode();
                NoncompliantCode sc = new NoncompliantCode();
            }
        }
    }
}
