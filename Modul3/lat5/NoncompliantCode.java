/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1_5;

/**
 *
 * @author dika
 */
public class NoncompliantCode {
    protected void doLogic(){
        System.out.println("Super Invoked");
    }
    
    public class Sub extends NoncompliantCode{
        public void doLogic(){
            System.out.println("Super Invoked");
        }
    }
}
