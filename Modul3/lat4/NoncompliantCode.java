/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1_4;

/**
 *
 * @author dika
 */
public class NoncompliantCode {
    public void readSensitiveFile(){
        try {
            SecurityManager sm = System.getSecurityManager();
            if (sm != null){
                sm.checkRead("/temp/tempFile");
            }
            //Access the file
        } catch (SecurityException se) {
            //Log exception
        }
    }
}
