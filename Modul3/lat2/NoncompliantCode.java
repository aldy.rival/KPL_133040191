/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1_2;

/**
 *
 * @author dika
 */
public class NoncompliantCode {
    public static int getAbsAdd(int x, int y){
        assert x != Integer.MIN_VALUE;
        assert y != Integer.MIN_VALUE;
        int absX = Math.abs(x);
        int absY = Math.abs(y);
        assert (absX <= Integer.MAX_VALUE - absY);
        return absX + absY;
    }
    
    public static void main(String[] args) {
        System.out.println(getAbsAdd(5, 6));
    }
}
