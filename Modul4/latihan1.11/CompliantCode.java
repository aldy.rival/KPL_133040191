/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1_11;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author dika
 */
public class CompliantCode {
    private static void receiveXMLStream(InputStream inStream, DefaultHandler defaultHandler) throws ParserConfigurationException, SAXException, IOException{
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        
        XMLReader reader = saxParser.getXMLReader();
        reader.setEntityResolver((EntityResolver) new CustomResolver());
        reader.setContentHandler(defaultHandler);
        
        InputSource is = new InputSource(inStream);
        reader.parse(is);
    }
    
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException{
        try{
            receiveXMLStream(new FileInputStream("evil.xml"), new DefaultHandler());
        } catch (java.net.MalformedURLException mue){
            System.err.println("Malformed URL Exception: " + mue);
        }
    }
}
