/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1_5;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author dika
 */
public class NoncompliantCode {
    static Calendar c = new GregorianCalendar(1995, GregorianCalendar.MAY, 23);
    public static void main(String[] args) {
        System.out.format(args[0], " did not match! HINT: It was issued on %l$terd of some month", c);
    }
}
