/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1_6;

import java.io.InputStream;

/**
 *
 * @author dika
 */
public class NoncompliantCode {
    public static void main(String[] args) throws Exception{
        String dir = System.getProperty("dir");
        Runtime rt = Runtime.getRuntime();
        Process proc  = rt.exec(new String[] {"sh", "-c", "ls " + dir});
        int result = proc.waitFor();
        if(result != 0){
            System.out.println("Process error: " + result);
        }
        InputStream in = (result == 0) ?proc.getInputStream() :
                                        proc.getErrorStream();
        int c;
        while((c = in.read()) != -1){
            System.out.println((char) c);
        }
    }
}
