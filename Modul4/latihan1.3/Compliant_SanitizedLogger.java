/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1_3;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author dika
 */
public class Compliant_SanitizedLogger {

    class SanitizedTextLogger extends Logger {

        Logger delegate;

        public SanitizedTextLogger(Logger delegate) {
            super(delegate.getName(), delegate.getResourceBundleName());
            this.delegate = delegate;
        }

        public String sanitize(String msg) {
            Pattern newline = Pattern.compile("\n");
            Matcher matcher = newline.matcher(msg);
            return matcher.replaceAll("\n  ");
        }

        public void severe(String msg) {
            delegate.severe(sanitize(msg));
        }

        // .. Other Logger methods which must also sanitize their log messages
    }
}
