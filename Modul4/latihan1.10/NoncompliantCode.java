/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1_10;

import java.io.BufferedOutputStream;
import java.io.IOException;

/**
 *
 * @author dika
 */
public class NoncompliantCode {
    private static void createXMLStreamBad(final BufferedOutputStream outStream, final String quantity) throws IOException{
        String xmlString = "<item>\n<description> Widget </description>\n"
                + "<price>500</price>\n" + "<quantity>" + quantity
                + "</quantity></item>";
        outStream.write(xmlString.getBytes());
        outStream.flush();
    } 
}
